# Sample Dockerfile to build a custom Jenkins slave image to be used
# with CERN Jenkins instances (cf. http://cern.ch/jenkinsdocs)

# Start from the base SLC6 or CC7 slave images
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
# Use tag 'slc6' instead of 'cc7' for SLC6
# The FROM statement can be overriden in the GitLab-CI build

# install packages
RUN yum install -y gcc-c++ make \
    && yum clean all \
    && g++ --version \
    && wget https://downloads.sourceforge.net/project/astyle/astyle/astyle%203.0.1/astyle_3.0.1_linux.tar.gz \
    && tar -zxf astyle_3.0.1_linux.tar.gz \
    && cd astyle/build/gcc \
    && make

# set path to include compiled executable
ENV PATH "$PATH:/astyle/build/gcc/bin/"
